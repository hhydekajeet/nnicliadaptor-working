package com.kajeet.nnicliadaptor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kajeet.nnicliadaptor.exception.InvalidInputException;
import com.kajeet.nnicliadaptor.exception.LoginException;
import com.kajeet.nnicliadaptor.exception.RecordNotFoundException;
import com.kajeet.nnicliadaptor.exception.SystemException;
import com.kajeet.nnicliadaptor.model.AclPayload;
import com.kajeet.nnicliadaptor.model.CodeStatusMessage;
import com.kajeet.nnicliadaptor.model.NniCliAdaptorEntity;
import com.kajeet.nnicliadaptor.service.NniCliAdaptorService;

@RestController
@RequestMapping(NniCliAdaptorController.URL_SLASH)
public class NniCliAdaptorController {
	
	public static final String URL_SLASH          = "/";
	public static final String URL_ROOT           = "";
	public static final String URL_GROUP          = "/kajeet";
	public static final String URL_PROVISIONING   = "/provisioning";
	public static final String URL_ACL            = "/acl";
	public static final String URL_GROUP_PROVISIONING_ACL = URL_GROUP + URL_PROVISIONING + URL_ACL;
	public static final String URL_EXEC_SSH       = URL_GROUP_PROVISIONING_ACL + "/execssh"; 
	public static final String PATH_VAR_ACL       = "acl_id";
	public static final String PATH_VAR_DEST_HOST ="destination_host";
	public static final String URL_ACL_VAR        = "/{acl_id}";
	public static final String URL_DEST_HOST_VAR  = "/{destination_host}";
	public static final String URL_ROUTES         = "/routes";
	public static final String URL_ACL_ROUTES     = URL_GROUP_PROVISIONING_ACL + URL_ACL_VAR + URL_ROUTES; 
//	public static final String URL_ACL_DEST_HOST  = URL_GROUP_PROVISIONING_ACL + URL_ACL_VAR + URL_DEST_HOST_VAR; 
	// /kajeet/provisioning/acl/{acl_id}/routes
	
	@Autowired
	NniCliAdaptorService service;
	
	@GetMapping (URL_ROOT)
	public String welcome() {
		return "Welcome to the Kajeet Network-to-Network Web Service!";
	}

	@PutMapping (URL_EXEC_SSH)
	public ResponseEntity<NniCliAdaptorEntity> execSsh() {
		String response = "Reponse stubbed out";
		NniCliAdaptorEntity entity = service.getEntity();
		entity = service.execSsh(entity);
		return ResponseEntity.ok().body(entity);
	}
	
	
	@PostMapping(URL_ACL_ROUTES)
	public ResponseEntity<CodeStatusMessage> postAcl(@PathVariable(PATH_VAR_ACL) String aclId, @RequestBody (required = true) AclPayload payload) 
			throws LoginException, InvalidInputException, SystemException, RecordNotFoundException { 
		
		return ResponseEntity.ok().body(service.postAcl(aclId, payload));
	}
	@PutMapping(URL_ACL_ROUTES)
	public ResponseEntity<CodeStatusMessage> putAcl(@PathVariable(PATH_VAR_ACL) String aclId, @RequestBody (required = true) AclPayload payload) 
			throws LoginException, InvalidInputException, SystemException, RecordNotFoundException {
		
		return ResponseEntity.ok().body(service.putAcl(aclId, payload));
	}
	@DeleteMapping(URL_ACL_ROUTES)
	public ResponseEntity<CodeStatusMessage> deleteAcl(@PathVariable(PATH_VAR_ACL) String aclId, @RequestBody (required = true) AclPayload payload) 
			throws LoginException, InvalidInputException, SystemException, RecordNotFoundException {
		
		return ResponseEntity.ok().body(service.deleteAcl(aclId, payload));
	}
	@GetMapping(URL_ACL_ROUTES)
	public ResponseEntity<AclPayload> getAcl(@PathVariable(PATH_VAR_ACL) String aclId) throws LoginException, InvalidInputException {
		
		return ResponseEntity.ok().body(service.getAclRoutes(aclId, null));
	}
	 
}


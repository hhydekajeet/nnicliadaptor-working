package com.kajeet.nnicliadaptor.exception;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Created by staylor on 8/14/15.
 */
public class GatewayError {
    private int code;
    private String message;
    private List<String> fields;

    public GatewayError(int code, String message, List<String> fields) {
        this.code = code;
        this.message = message;
        this.fields = fields;
    }

    public GatewayError(int code, String message) {
        this.code = code;
        this.message = message;
        this.fields = new ArrayList<String>();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GatewayError)) return false;

        GatewayError that = (GatewayError) o;

        if (getCode() != that.getCode()) return false;
        if (!getMessage().equals(that.getMessage())) return false;
        return getFields().equals(that.getFields());

    }

    @Override
    public int hashCode() {
        int result = getCode();
        result = 31 * result + getMessage().hashCode();
        result = 31 * result + getFields().hashCode();
        return result;
    }
    
	@Override
    public String toString() {
        return String.format("code='%d'  message='%s'  fields='%s'", this.getCode(), this.getMessage(), StringUtils.join(this.fields));
    }
    
    @SuppressWarnings("rawtypes")
	public static ResponseEntity doTestResponse(String customerId, String message) {
    	HttpStatus status = null;
    	String httpCode = null;
    	String errorCode = null;
    	try { 
	    	String testId = customerId.substring(0, 2);
	    	if(!testId.equals("11"))
	    		return null;
	    	
	    	httpCode = customerId.substring(2, 5);
	    	errorCode = customerId.substring(5, customerId.length());
	    	//Just in case: Ignore all 200 status codes, convert to 404 since the customer should be an invalid one
	    	if(httpCode.startsWith("2")) {
	    		httpCode = "404";
	    		errorCode = "404";
	    		message = "Customer not found";
	    	}
    
    		status = HttpStatus.valueOf(Integer.parseInt(httpCode));
    	} catch (Exception ex) {
    		status = HttpStatus.INTERNAL_SERVER_ERROR;
    	}
    	GatewayError gwError = null;
    	if(StringUtils.isEmpty(message))
    		message = status.getReasonPhrase();
    	if(!StringUtils.isEmpty(errorCode) &&! errorCode.startsWith("0"))
    		gwError = new GatewayError(Integer.parseInt(errorCode), message);
    	else
    		gwError = new GatewayError(status.value(), message);
    	 
    	return new ResponseEntity<>(gwError, status);
    }
}

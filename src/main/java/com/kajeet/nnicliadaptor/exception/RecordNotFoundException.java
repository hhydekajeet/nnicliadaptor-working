package com.kajeet.nnicliadaptor.exception;

/**
 * Created by Lawrence Matta on 8/25/15.
 */
public class RecordNotFoundException extends Exception {
	private static final long serialVersionUID = 4578645147565050727L;

	public RecordNotFoundException(String message) {
        super(message);
    }
}

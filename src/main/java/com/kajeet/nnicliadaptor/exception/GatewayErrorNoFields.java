package com.kajeet.nnicliadaptor.exception;

/**
 * Created by Fernando Del Valle on 12/21/2020.
 */
public class GatewayErrorNoFields {
	private String errorCode;
	private String errorMessage;

	public GatewayErrorNoFields(String message, String resultCode) {
		this.errorMessage = message;
		this.errorCode = resultCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		return String.format("errorCode='%s'  errorMessage='%s' ", this.getErrorCode(), this.getErrorMessage());
	}
}

package com.kajeet.nnicliadaptor.exception;

/**
 * Created by Lawrence Matta on 9/3/15.
 */
public class LoginException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8853976217204974724L;
	
    public LoginException(String message) {
        super(message);
    }

}

package com.kajeet.nnicliadaptor.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Lawrence Matta on 9/8/15.
 */
public class SystemException extends Exception {
	private static final long serialVersionUID = -6975161036980064042L;

    public SystemException(String message) {
        super(message);
    }

	public SystemException(Exception cause)
    {
        super(cause);
    }

    public SystemException(String message, Exception cause) {
        super(message, cause);
    }

    public String toStackTrace() {
        StringWriter errors = new StringWriter();
        super.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }
}

package com.kajeet.nnicliadaptor.exception;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
 
/**
 * Adapted and stripped down from staylor's DaylightAPI version by HHyde on 3/8/2021.
 */

@ControllerAdvice()
public class GatewayResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger log = (Logger) LogManager.getLogger(GatewayResponseEntityExceptionHandler.class);
	//Logger log = LoggerFactory.getLogger(GatewayResponseEntityExceptionHandler.class);
	
	
    @ExceptionHandler(InvalidInputException.class)
    public ResponseEntity<GatewayError> handleInvalidInputException(HttpServletRequest req, InvalidInputException ex) {
        GatewayError gatewayError = new GatewayError(HttpStatus.METHOD_NOT_ALLOWED.value(), ex.getLocalizedMessage(), null);
        gatewayError.setFields(ex.getFields());
        log.warn("InvalidInputException: " + gatewayError.toString());
        return new ResponseEntity<>(gatewayError, HttpStatus.METHOD_NOT_ALLOWED);
    }
    
    @ExceptionHandler(RecordNotFoundException.class)
    public ResponseEntity<GatewayError> handleRecordNotFoundException(HttpServletRequest req, RecordNotFoundException ex) {
        GatewayError gatewayError = new GatewayError(HttpStatus.NOT_FOUND.value(), ex.getLocalizedMessage(), null);
        log.warn("RecordNotFoundException: " + gatewayError.toString());
        return new ResponseEntity<>(gatewayError, HttpStatus.NOT_FOUND);
    }
    
    @ExceptionHandler(LoginException.class)
    public ResponseEntity<GatewayError> handleLoginException(HttpServletRequest req, LoginException ex) {
        GatewayError gatewayError = new GatewayError(HttpStatus.UNAUTHORIZED.value(), ex.getLocalizedMessage(), null);
        log.warn("LoginException: " + gatewayError.toString());
        return new ResponseEntity<>(gatewayError, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(SystemException.class)
    public ResponseEntity<GatewayError> handleSystemExceptiion(HttpServletRequest req, SystemException ex) {
    	String message = ex.getCause() != null && !StringUtils.isEmpty(ex.getCause().getLocalizedMessage()) ? ex.getCause().getLocalizedMessage() : ex.getLocalizedMessage();
    	int errorCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
        GatewayError gatewayError = new GatewayError(errorCode, message, null);
        log.error("SystemException: " + gatewayError.toString());
        return new ResponseEntity<>(gatewayError, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    

    // Example of exception handler that comes with ResponseEntityExceptionHandler
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        GatewayError gatewayError = new GatewayError(status.value(), ex.getLocalizedMessage(),null);
        log.warn("HttpRequestMethodNotSupportedException: " + gatewayError.toString());
        return new ResponseEntity<Object>(gatewayError, headers, status);
    }
    
    @Override
    protected ResponseEntity<Object>handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        GatewayError gatewayError = new GatewayError(status.value(), ex.getLocalizedMessage(), null);
        log.warn("MissingServletRequestParameterException: " + gatewayError.toString());
        return new ResponseEntity<Object>(gatewayError, headers, status);
    }

    @Override
    protected ResponseEntity<Object>handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String resourceNotFoundMsg = "Resource not found: " + ex.getRequestURL();
        GatewayError gatewayError = new GatewayError(status.value(), resourceNotFoundMsg);
        gatewayError.getFields().add("URL");
        log.warn("NoHandlerFoundException: " + gatewayError.toString());
        return new ResponseEntity<Object>(gatewayError, headers, status);
    }

    @Override
    protected ResponseEntity<Object>handleMissingPathVariable(MissingPathVariableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        GatewayError gatewayError = new GatewayError(status.value(), ex.getLocalizedMessage(), null);
        log.info("MissingPathVariableException: " + gatewayError.toString());
        return new ResponseEntity<Object>(gatewayError,status);
    }
    
    @Override
    protected ResponseEntity<Object>handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        status = HttpStatus.METHOD_NOT_ALLOWED;
    	GatewayError gatewayError = new GatewayError(status.value(), ex.getLocalizedMessage(), null);
        log.warn("TypeMismatchException: " + gatewayError.toString());
        return new ResponseEntity<Object>(gatewayError,status);
    }

    @Override
    protected ResponseEntity<Object>handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        status = HttpStatus.METHOD_NOT_ALLOWED;
        GatewayError gatewayError = new GatewayError(status.value(), ex.getLocalizedMessage(), null);
        log.warn("HttpMessageNotReadableException: " + gatewayError.toString());
        return new ResponseEntity<Object>(gatewayError,status);
    }
    
}

package com.kajeet.nnicliadaptor;
 
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.kajeet.nnicliadaptor.cidr.Cidr;

@SpringBootApplication
public class NniCliAdaptorApplication {
	
//	static void testSomeVars (String[] stringArray, String singleString, AclRoute route, AclRoute[] routeArray) {
//		stringArray[0] = "Inside"; 
//		singleString = new String("Single String Inside"); 
//		route = new AclRoute();
//		route.setDestination_host("Destination Inside");
//		route.setSource_cidr("Source cidr Inside");
//		routeArray[0] = route;
//	}
//
	public static void main(String[] args) throws Exception {

		//		// THROW-AWAY TESTING: REMOVE
//		String[] stringArray = new String[1];
//		stringArray[0] = "Outside";
//		String singleString = "Outside Single String";
//		AclRoute route = new AclRoute();
//		route.setDestination_host("Destination OUTSIDE");
//		route.setSource_cidr("Source cidr OUTSIDE");
//		AclRoute[] routeArray = new AclRoute[1];
//		routeArray[0] = route;
//		
//		testSomeVars (stringArray, singleString, route, routeArray);
//		System.out.println("stringArray[0] = " + stringArray[0]);
//		System.out.println("singleString = " + singleString);
//		System.out.println("route.getDestination_host() = " + route.getDestination_host());
//		System.out.println("route.getSource_cidr() = " + route.getSource_cidr());
//		System.out.println("route.getDestination_host() = " + routeArray[0].getDestination_host());
//		System.out.println("route.getSource_cidr() = " + routeArray[0].getSource_cidr());
		
		boolean isValid = false;
		String ipAddressToValidate = "8.0.255.1";
		String subnetMaskToValidate = "0.0.1.255";
		isValid = Cidr.isValidIpAddress(ipAddressToValidate, subnetMaskToValidate);
		System.out.println("Cidr.isValidIpAddress(" + ipAddressToValidate + ", " + subnetMaskToValidate + ") is " + isValid);
		
		ipAddressToValidate = "8.8.0.0";
		subnetMaskToValidate = "0.0.1.255";
		isValid = Cidr.isValidIpAddress(ipAddressToValidate, subnetMaskToValidate);
		System.out.println("Cidr.isValidIpAddress(" + ipAddressToValidate + ", " + subnetMaskToValidate + ") is " + isValid);
		
		ipAddressToValidate = "8.0.0.1";
		subnetMaskToValidate = "0.0.0.255";
		isValid = Cidr.isValidIpAddress(ipAddressToValidate, subnetMaskToValidate);
		System.out.println("Cidr.isValidIpAddress(" + ipAddressToValidate + ", " + subnetMaskToValidate + ") is " + isValid);
		
		
		System.out.println("Cidr.isValidAclId(\"HowardTest1\") is " + Cidr.isValidAclId("HowardTest1"));
		System.out.println("Cidr.isValidAclId(\"Howard-Test1\") is " + Cidr.isValidAclId("Howard-Test1"));
		System.out.println("Cidr.isValidAclId(\"Howard$Test1\") is " + Cidr.isValidAclId("Howard$Test1"));
		System.out.println("Cidr.isValidAclId(\"Howard@Test1\") is " + Cidr.isValidAclId("Howard@Test1"));
		System.out.println("Cidr.isValidAclId(\"HowardTest1$\") is " + Cidr.isValidAclId("HowardTest1$"));
		System.out.println("Cidr.isValidAclId(\"-HowardTest1\") is " + Cidr.isValidAclId("-HowardTest1"));
		System.out.println("Cidr.isValidAclId(\"HowardTest1-\") is " + Cidr.isValidAclId("HowardTest1-"));
		
		
		
		//
		SpringApplication.run(NniCliAdaptorApplication.class, args);
	}
}

package com.kajeet.nnicliadaptor.cidr;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.kajeet.nnicliadaptor.cisco.CiscoCliAdaptorService;
import com.kajeet.nnicliadaptor.exception.InvalidInputException;
import com.kajeet.nnicliadaptor.model.AclRoute;

public class Cidr {
	
	  private static final Logger log = (Logger) LogManager.getLogger(Cidr.class);

	  public static String PERMIT_IP = "permit ip"; 
	public static String HOST = "host";
	public static char CHAR_ONE = '1';
	public static char CHAR_COMMA = ',';
	public static char CHAR_PERIOD = '.';
	public static char CHAR_SPACE = ' ';
	public static String STRING_COMMA = ",";
	public static String STRING_PERIOD = ".";
	public static int IP_ADDRESS_FIELDS = 4;
	public static int IP_ADDRESS_FIELD_LOWER_BOUND = 0;
	public static int IP_ADDRESS_FIELD_UPPER_BOUND = 255;
	public static String ZERO = "0";
	public static String NOTHING = "";
	public static char SLASH = '/';
	public static int MAX_BINARY_LENGTH = 32;
	public static String MESSAGE_BAD_IP_ADDRESS = "Bad IP Address with Mask";
	public static String MESSAGE_BAD_ACL_ID = "Bad ACL ID";
	public static String MESSAGE_BAD_CIDR_IP = "Bad CIDR IP address; Expected {x.x.x.x/yy}, got ";
	
	
	  public static boolean isValidAclId (String aclId) {
		  log.info("isValidAclId("+aclId+")");
		  String regex = "^[a-zA-Z0-9-]+$";
	      Pattern pattern = Pattern.compile(regex);
	      Matcher matcher = pattern.matcher(aclId);
		  log.info(matcher.matches());
		  return matcher.matches();
	  }
	  

	
	// Does the opposite of getInverseSubnetMask; Takes as subnet mask and returns a number between 0 AND 32;
	public static int getYyFromInverseSubnetMask (String subnetMask) {
		log.trace(String.format("Cidr.getYyFromInverseSubnetMask %s", subnetMask));
		int yy = 0;
		String mask = subnetMask.replace(CHAR_PERIOD, CHAR_COMMA);
		String[] fields = mask.split(STRING_COMMA);
		String ones = "";
		for (String field: fields) {
			ones = Integer.toBinaryString(Integer.valueOf(field)).replace(ZERO, NOTHING);
			yy += ones.length();
		}
		return MAX_BINARY_LENGTH-yy;
	}
	
	
	
	public static String getInverseSubnetMask(String ipAddress) throws NumberFormatException, InvalidInputException {
		log.trace(String.format("Cidr.getInverseSubnetMask %s", ipAddress));
		//System.out.println ("ipAddress = " + ipAddress);
		byte yy = 0;
		try {
			yy = Byte.valueOf(ipAddress.substring(ipAddress.indexOf(SLASH)+1));
		} catch (NumberFormatException x) {
			throw new InvalidInputException(MESSAGE_BAD_CIDR_IP + ipAddress);
		}
		
		if (yy <0 ||yy>MAX_BINARY_LENGTH) throw new InvalidInputException("Value error! /yy value must be between 0 and 32");
		StringBuilder binaryMask = new StringBuilder();
		StringBuilder mask = new StringBuilder();
		for (byte i = 0; i<yy; i++) {
			if (i>0 && i%8==0) binaryMask.append(CHAR_COMMA);
			binaryMask.append('0');
		}
		for (byte z = yy; z<MAX_BINARY_LENGTH; z++) {
			if (z>0 && z%8==0) binaryMask.append(CHAR_COMMA);
			binaryMask.append(CHAR_ONE);
		}
		String[] fields = binaryMask.toString().split(STRING_COMMA);
		Integer[] intFields = new Integer[4]; //fields.length];
		
		for (int i=0; i<fields.length; i++) {
			intFields[i] = Integer.parseInt(fields[i], 2);
		}
		
		int countDots = 0;
		for (Integer intField: intFields) {
			countDots++;
			mask.append(intField);
			if (countDots<4) mask.append(CHAR_PERIOD);
		}
		return mask.toString().replaceAll(STRING_COMMA, STRING_PERIOD);
	}
	
	public static boolean validateIpAddressFormat (String ipAddress) throws NumberFormatException {
		log.trace(String.format("Cidr.validateIpAddressFormat %s", ipAddress));
		String address = ipAddress.replace (STRING_PERIOD, STRING_COMMA);
		String[] addressFields = address.split(STRING_COMMA);
		if (addressFields.length != IP_ADDRESS_FIELDS) return false;
		for (String field: addressFields) {
			if (Integer.valueOf(field) < IP_ADDRESS_FIELD_LOWER_BOUND || Integer.valueOf(field) > IP_ADDRESS_FIELD_UPPER_BOUND) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean isValidSubnetMask (String subnetMask) throws InvalidInputException {
		log.trace(String.format("Cidr.isValidSubnetMask %s", subnetMask));
		String mask = subnetMask.replace(CHAR_PERIOD, CHAR_COMMA);
		String[] fields = mask.split(STRING_COMMA);
		if (fields.length != 4) return false; 
		int fieldValue = 0;
		for (String field: fields) {
			if (fieldValue>255 || fieldValue<0) return false;
		}		
	  return true;
	}

	
	public static boolean isValidIpAddress (String ipAddress, String subnetMask) throws InvalidInputException {
		log.trace(String.format("Cidr.isValidIpAddress %s %s", ipAddress, subnetMask));
		if (!validateIpAddressFormat(ipAddress) || !isValidSubnetMask(subnetMask)) throw new InvalidInputException(MESSAGE_BAD_IP_ADDRESS);
		// TODO: JUNIT 
		String mask = subnetMask.replace(CHAR_PERIOD, CHAR_COMMA);
		String[] subnetFields = mask.split(STRING_COMMA);
		ipAddress = ipAddress.replace(CHAR_PERIOD, CHAR_COMMA);
		String[] addressFields = ipAddress.split(STRING_COMMA);
		if (subnetFields.length != 4 || addressFields.length != 4) return false; 
		char[] subnetCharField = null;
		char[] addressCharField = null;
		//String workingField = null;
		for (int i=addressFields.length-1; i>=0; i--) {
			subnetCharField= Integer.toBinaryString(Integer.valueOf(subnetFields[i])).toCharArray();
			addressCharField= Integer.toBinaryString(Integer.valueOf(addressFields[i])).toCharArray();
			//if (subnetCharField.length )
			for (int j=subnetCharField.length-1, k=addressCharField.length-1; j>=0 && k>=0; j--, k--) {
				if (subnetCharField[j] == CHAR_ONE && addressCharField[k] == CHAR_ONE)
						return false;
			}
		}
	  return true;
	}
	
	
	public static List<String> parseIpAddresses (String containsIpAddresses) {
		// THIS METHOD IS FAULTY; CANNOT RAPIDLY DETERMINE THAT THERE ARE NO IP ADDRESSES TO BE FOUND AND ABORT.
		log.trace(String.format("Cidr.parseIpAddresses %s", containsIpAddresses));
		List<String> stringList = new ArrayList<>();
		String candidateIpAddress = "";
		int startIndex = 0;
		int currentIndex = 0;
		int endIndex = containsIpAddresses.length();
		int afterNextIndexOfSpace = -1;
		int previousAfterNextIndexOfSpace = -1;
		
		int nextIndexOfPeriod = -1;
		int nextIndexOfSpace = -1;
		int iterations = 0;
		
		do {
			iterations++;
			candidateIpAddress = "";
			nextIndexOfPeriod = containsIpAddresses.indexOf(CHAR_PERIOD, currentIndex); // Period 1
			if (nextIndexOfPeriod<0) return null;
			nextIndexOfSpace = containsIpAddresses.indexOf(CHAR_SPACE, currentIndex);
			afterNextIndexOfSpace = nextIndexOfSpace;
			
			if (nextIndexOfSpace < (nextIndexOfPeriod-1)) {
				while (afterNextIndexOfSpace < nextIndexOfPeriod && afterNextIndexOfSpace >= 0) {
					previousAfterNextIndexOfSpace = afterNextIndexOfSpace;
					afterNextIndexOfSpace = containsIpAddresses.indexOf(CHAR_SPACE, afterNextIndexOfSpace+1);	
				}
				startIndex = previousAfterNextIndexOfSpace + 1;
			}
			currentIndex = nextIndexOfPeriod+1;
			nextIndexOfPeriod = containsIpAddresses.indexOf(CHAR_PERIOD, currentIndex); // Period 2
			currentIndex = nextIndexOfPeriod+1;
			nextIndexOfPeriod = containsIpAddresses.indexOf(CHAR_PERIOD, currentIndex); // Period 3
			currentIndex = nextIndexOfPeriod+1;
			candidateIpAddress = containsIpAddresses.substring(startIndex, afterNextIndexOfSpace<0?endIndex:afterNextIndexOfSpace);
			stringList.add(candidateIpAddress);
			nextIndexOfSpace = containsIpAddresses.indexOf(CHAR_SPACE, currentIndex);
		} while (nextIndexOfSpace > currentIndex && iterations<1000); // REFINE THIS SAFETY VALVE
		return stringList;
	}
	
	
	
	public static AclRoute getRoute(String permitIpXxYyHostZz) throws Exception {
		log.trace(String.format("Cidr.getRoute %s", permitIpXxYyHostZz));
		// permitIpXxYyHostZz is a string in the format "00 permit ip x.x.x.x y.y.y.y host z.z.z.z"
		// It is the result of the command "sh access-list <acl_id>"
		AclRoute route = new AclRoute();
		List<String> ipAddressList = parseIpAddresses(permitIpXxYyHostZz);
		for(String ipAddress: ipAddressList) {
			if (!(validateIpAddressFormat(ipAddress))) {
				throw new Exception ("ipAddress " + ipAddress + " failed validation.");
			}
		}
		int index = 0;
		String ipAddress1 = "";
		for(String ipAddress: ipAddressList) {
			index++;
			//System.out.println("ipAddress " + index + ": " + ipAddress);
			switch (index) {
			case 1: {
				route.setSource_cidr(ipAddress);
				ipAddress1 = ipAddress;
				break;
			}
//			case 2: route.setSource_cidr(route.getSource_cidr() + "/" + ipAddress);
			case 2: {
				route.setSource_cidr(ipAddress1 + SLASH + getYyFromInverseSubnetMask(ipAddress));
				break;
			}
			case 3: {
				route.setDestination_host(ipAddress);
				break;
			}
			default: {
				System.out.println("Cidr.getRoute() reached an unexpected condition: more than 3 IP addresses in a permit statement.");
				break;
			}
			}
				
		}
		//System.out.println("");
		return route;
	}
	

}

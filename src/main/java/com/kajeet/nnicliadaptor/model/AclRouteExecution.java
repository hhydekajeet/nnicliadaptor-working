package com.kajeet.nnicliadaptor.model;

public class AclRouteExecution {
	
  private AclRoute route;
  private AclExecution execution;
  
  public AclRouteExecution() {}
  public AclRouteExecution(AclRoute route, AclExecution execution) {
	super();
	this.route = route;
	this.execution = execution;
  }
	
  public AclRoute getRoute() {
	return route;
  }
  
	public void setRoute(AclRoute route) {
		this.route = route;
	}
	public AclExecution getExecution() {
		return execution;
	}
	public void setExecution(AclExecution execution) {
		this.execution = execution;
	}
	
}

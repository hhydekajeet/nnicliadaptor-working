package com.kajeet.nnicliadaptor.model;

public class NniCliAdaptorEntity {
	
	private long id;
	private String shortName;
	private String longName;
	private String description;
	private String notes;
	
	
	public NniCliAdaptorEntity() {}
		
	public NniCliAdaptorEntity(long id, String shortName, String longName, String description, String notes) {
		super();
		this.id = id;
		this.shortName = shortName;
		this.longName = longName;
		this.description = description;
		this.notes = notes;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getLongName() {
		return longName;
	}
	public void setLongName(String longName) {
		this.longName = longName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}


}

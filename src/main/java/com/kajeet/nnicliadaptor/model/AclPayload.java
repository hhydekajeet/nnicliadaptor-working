package com.kajeet.nnicliadaptor.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class AclPayload {
	
	
	List<AclRoute> routes;

	public List<AclRoute> getRoutes() {
		return routes;
	}
	
	@JsonIgnore
	public Map<String, AclRoute> getRouteMap() {
		Map<String, AclRoute> routeMap = new TreeMap();
		for (AclRoute route: routes) {
			routeMap.put(route.getKey(), route);
		}
		return routeMap;
	}

	public void setRoutes(List<AclRoute> routes) {
		this.routes = routes;
	}
	
	public static AclPayload getDummyAclPayload() {
		AclRoute route = new AclRoute();
		route.setDestination_host("{z.z.z.z}");
		route.setSource_cidr("{x.x.x.x}/{y}");
		AclPayload payload = new AclPayload();
		List<AclRoute> routes = new ArrayList<>();
		routes.add(route);
		payload.setRoutes(routes);
	    return payload;
	}
}

package com.kajeet.nnicliadaptor.model;

import java.util.ArrayList;
import java.util.List;

public class AclResult {

	private List<AclRouteExecution> routeExecutions;

	public AclResult() {
		super();
		this.routeExecutions = new ArrayList<>();
	}

	public AclResult(List<AclRouteExecution> routeExecutions) {
		super();
		this.routeExecutions = routeExecutions;
	}

	public List<AclRouteExecution> getRouteExecutions() {
		return routeExecutions;
	}

	public void setRouteExecutions(List<AclRouteExecution> routeExecutions) {
		this.routeExecutions = routeExecutions;
	}
	
	public void addRouteExecution(AclRouteExecution routeExecution) {
		this.routeExecutions.add(routeExecution);
	}
	
	
}

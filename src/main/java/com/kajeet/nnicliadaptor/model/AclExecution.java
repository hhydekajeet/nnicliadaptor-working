package com.kajeet.nnicliadaptor.model;

public class AclExecution {
	
	// This class was developed early in the Dev/Testing cycle and has been neglected since it ceased being used as a return value.
	// Consider dropping it and all code referencing it. --Howard Hyde, 2021.03.12
	
	private String command;
	private String answer;
		
	public AclExecution() {}
	public AclExecution(String command, String answer) {
		super();
		this.command = command;
		this.answer = answer;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	

}

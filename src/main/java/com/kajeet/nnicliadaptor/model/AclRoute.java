package com.kajeet.nnicliadaptor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class AclRoute {
	
	private String source_cidr;
	private String destination_host;
	
	public String getSource_cidr() {
		String returnVal = source_cidr.replace("{", "");
		return returnVal.replace("}", "");
	}
	public String getDestination_host() {
		String returnVal = destination_host.replace("{", "");
		return returnVal.replace("}", "");
	}
		
	public void setSource_cidr(String source_cidr) {
		this.source_cidr = source_cidr;
	}
	public void setDestination_host(String destination_host) {
		this.destination_host = destination_host;
	}
	
	@JsonIgnore
	public String getKey() {
		return this.destination_host.concat(" | ".concat(this.source_cidr));
	}
	
	@Override
	public int hashCode() {
		return this.destination_host.hashCode();
	}
	
	@Override
	public boolean equals(Object obj){
		
		if(obj == null || this.destination_host == null || this.source_cidr == null){
			return false;
		}
		if(obj instanceof AclRoute && this == obj) {
			return true;
		}
		
		AclRoute other = (AclRoute)obj;
		
		if(other.destination_host == null || other.source_cidr == null){
			return false;
		}
		
		if(this.destination_host.equals(other.destination_host) && this.source_cidr.equals(other.source_cidr)){
			return true;
		}
		
		return false;
	}	

}

package com.kajeet.nnicliadaptor.service;

import com.kajeet.nnicliadaptor.exception.InvalidInputException;
import com.kajeet.nnicliadaptor.exception.LoginException;
import com.kajeet.nnicliadaptor.exception.RecordNotFoundException;
import com.kajeet.nnicliadaptor.exception.SystemException;
import com.kajeet.nnicliadaptor.model.AclPayload;
import com.kajeet.nnicliadaptor.model.CodeStatusMessage;
import com.kajeet.nnicliadaptor.model.NniCliAdaptorEntity;
import com.kajeet.nnicliadaptor.net.Conversation;

public interface NniCliAdaptorService {
	
	// TODO: Review Exception declarations and handling

	NniCliAdaptorEntity getEntity();
	NniCliAdaptorEntity execSsh(NniCliAdaptorEntity entity);
	
	CodeStatusMessage postAcl (String aclId, AclPayload payload) throws LoginException, InvalidInputException, SystemException, RecordNotFoundException;
	CodeStatusMessage putAcl (String aclId, AclPayload payload) throws LoginException, InvalidInputException, SystemException, RecordNotFoundException;
	CodeStatusMessage deleteAcl (String aclId, AclPayload payload) throws LoginException, InvalidInputException, SystemException, RecordNotFoundException;
	AclPayload getAclRoutes (String aclId, Conversation conversation) throws LoginException, InvalidInputException;
		
}

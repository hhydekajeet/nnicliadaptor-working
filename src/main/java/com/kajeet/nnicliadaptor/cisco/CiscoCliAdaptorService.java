package com.kajeet.nnicliadaptor.cisco;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.kajeet.nnicliadaptor.cidr.Cidr;
import com.kajeet.nnicliadaptor.exception.InvalidInputException;
import com.kajeet.nnicliadaptor.exception.LoginException;
import com.kajeet.nnicliadaptor.exception.RecordNotFoundException;
import com.kajeet.nnicliadaptor.exception.SystemException;
import com.kajeet.nnicliadaptor.model.AclExecution;
import com.kajeet.nnicliadaptor.model.AclPayload;
import com.kajeet.nnicliadaptor.model.AclResult;
import com.kajeet.nnicliadaptor.model.AclRoute;
import com.kajeet.nnicliadaptor.model.AclRouteExecution;
import com.kajeet.nnicliadaptor.model.CodeStatusMessage;
import com.kajeet.nnicliadaptor.model.NniCliAdaptorEntity;
import com.kajeet.nnicliadaptor.net.Conversation;
import com.kajeet.nnicliadaptor.service.NniCliAdaptorService;

//import inet.ipaddr.IPAddress;
//import inet.ipaddr.IPAddressString;

@Service
public class CiscoCliAdaptorService implements NniCliAdaptorService{

  private static final Logger log = (Logger) LogManager.getLogger(CiscoCliAdaptorService.class);

  @Value("${kajeet.nni.ssh.target.host}")
  private String host = "10.101.1.5";

  @Value("${kajeet.nni.ssh.target.user}")
  private String user = "NNI_user";

  @Value("${kajeet.nni.ssh.target.pass}")
  private String pass = "NNI_password";

  @Value("${kajeet.nni.ssh.target.port}")
  private int port = 22;

  @Value("${kajeet.nni.ssh.prompt.magic-char}")
  private String promptMagicChar = "#";

  public static final String STRICT_HOSTKEY_CHECKING    = "StrictHostKeyChecking";
  public static final String STRICT_HOSTKEY_CHECKING_NO = "no";
  public static final String EXEC                       = "exec";
  public static final String SHELL                      = "shell";
  public static final String SSH_SUCCESS                = "Success! ";
  public static final String SSH_FAIL                   = "FAIL! ";
  public static final String SSH_UNKNOWN                = "Unknown. ";
  public static final String SSH_NOT_EXEC               = "Not executed";
  public static final String CIDR_XXXXXXXX              = "XXXXXXXX";
  public static final String CIDR_YYYYYYYY              = "YYYYYYYY";
  public static final String CIDR_ZZZZZZZZ              = "ZZZZZZZZ"; 
//  public static final String ACL_NO_PERMIT              = "no permit "; 
  public static final String ACL_PERMIT                 = "permit "; 
  public static final String ACL_COMMANDS_EXECUTED      = "commands executed"; 
  public static final String ENTRIES_ALREADY_EXIST      = "entries already exist"; 
  public static final String PHRASE_SEPARATOR           = "; "; 
  public static final String SPACE                      = " "; 
  public static final String HOST                      = "host"; 
  public static final String NEWLINE                    = "\\r?\\n";
  public static final String SOMETHING_WRONG            = "Sorry, something unexpected happened: ";
  public static final String CONVERSATION_UNABLE        = "Unable to start conversation with target host.";
  public static final String REVERSE_DNS_SUFFIX         = ".in-addr.arpa"; // Is this a permanent/industry-standard value, or a transient accident?

  public static final int INDEX_ZERO                    = 0; 
  public static final int INDEX_ONE                     = 1; 
  public static final int INDEX_TWO                     = 2; 
  public static final int INDEX_THREE                   = 3; 
  public static final int INDEX_FOUR                    = 4; 
  public static final int INDEX_FIVE                    = 5; 
  
  
  public static final int ONE                           = 1; 
  public static final int TEN                           = 10; 
  public static final int ONE_HUNDRED                   = 100; 
  public static final int ONE_THOUSAND                  = 1000; 
  
  //public static final int WAIT_TIMEOUT_PAD              = 10; 
  public static final int WAIT_TIMEOUT_PUT              = TEN; 
  public static final int WAIT_TIMEOUT_GET              = TEN; 

  // Should these 3 be an enum?
  public static final String ACTION_POST               = "POST";
  public static final String ACTION_DELETE             = "DELETE";
  public static final String ACTION_PUT                = "PUT";
  
  public static final String ACTION_POST_PREFIX        = "";
  public static final String ACTION_DELETE_PREFIX      = "no";
  public static final String MESSAGE_PERMIT            = " [permit] ";
  public static final String MESSAGE_NO_PERMIT         = " [no permit] ";
  public static final String MESSAGE_RECORD_NOT_FOUND  = "Record not found: ";
  

  
  @Override
  public NniCliAdaptorEntity getEntity() { // INITIAL DEV/TESTING PURPOSES; DEPRECATED
    return new NniCliAdaptorEntity(1001, "Cisco Entity", "Cisco Systems Entity", "Indescribable", "No notes to note, he noted in his notebook");
  }

  @Override
  public NniCliAdaptorEntity execSsh (NniCliAdaptorEntity entity) {
    String result = testSsh();
    entity.setNotes(result);

    return entity;
  }

  private String testSsh () {
    String response = "testSsh() result = [";
    String[] commands = new String[2];
    String[] results  = new String[2];
    //    commands[0] = "sh int GigabitEthernet0/0/1 | include Description";
    commands[1] = "sh access-list";
    //    commands[0] = "sh access-list peter-test2 | inc 10.99.96.1\r\nsh access-list peter-test2";
    commands[0] = "sh access-list peter-test2 | inc 10.99.96.1";
    results[0] = SSH_NOT_EXEC;
    results[1] = SSH_NOT_EXEC;
    String result = execSshCommands(user, pass, host, port, commands, results);
    String resultsString = String.join("\r\n", results);
    response += (resultsString + "]");
    return response;
  }

  String waitForPrompt(ByteArrayOutputStream outputStream, String command, int waitTimeout) throws InterruptedException {
    int retries = ONE_HUNDRED; 
    String outputString = outputStream.toString();
    log.trace("CiscoCliAdaptorService.waitForPrompt(" + outputString + ")");
    int indexOf1stMagicChar = 0;
    String searchString = ""+promptMagicChar+command.substring(0,3);
    int x = 0;
    for (x = 0; x < retries; x++) {
      Thread.sleep(waitTimeout);
      outputString = outputStream.toString(); // For debug tracing here
      indexOf1stMagicChar = outputStream.toString().indexOf(searchString)+1;
      if (outputStream.toString().indexOf(promptMagicChar, indexOf1stMagicChar) > 0) { // #
          Thread.sleep(waitTimeout);
	      outputString = outputStream.toString();
	      outputStream.reset();
	      log.trace("waitForPrompt outputString = " + outputString);
	      log.info("CiscoCliAdaptorService.waitForPrompt(" + x + ")");
	      break;
      }
    }
    return outputString;
//    return "ERROR! CiscoCliAdaptorService.waitForPrompt() may have timed out.";
  }

  private String execSshCommands(String username, String password, 
      String host, int port, String[] commands, String[] responses) { // THIS METHOD HAS ONLY BEEN USED FOR EARLY DEV/TESTING PURPOSES.
    String responseString = "";

    Session session = null;
    //    ChannelExec channel = null;
    ChannelShell channel = null;
    ByteArrayOutputStream responseStream = new ByteArrayOutputStream();

    int index = -1;
    try {
      session = new JSch().getSession(username, host, port);
      session.setPassword(password);
      session.setConfig(STRICT_HOSTKEY_CHECKING, STRICT_HOSTKEY_CHECKING_NO);
      session.connect();
      channel = (ChannelShell) session.openChannel(SHELL);
      //        channel = (ChannelExec) session.openChannel(EXEC);
      PrintStream stream = new PrintStream(channel.getOutputStream());
      //responseStream = new ByteArrayOutputStream();
      channel.setOutputStream(responseStream);
      stream = new PrintStream(channel.getOutputStream());

      String commandResponse = "";  
      for (String command: commands) { 
        index++;
        if (index==0) channel.connect();
        responseString = SSH_UNKNOWN;
        log.info("log.info(" + command + ")");

        stream.println(command);
        stream.flush();
        responseString = SSH_SUCCESS;
        commandResponse = waitForPrompt(responseStream, command, WAIT_TIMEOUT_PUT);            

        responses[index] = SSH_SUCCESS + commandResponse;
        //responseString += (responseStream.toByteArray());
      }
    } catch (Exception x) {
      if (index < 0) index=0;
      responses[index] = SSH_FAIL + "Exception: " + x.getMessage();
      responseString = SSH_FAIL;
    } finally {
      log.trace(responseString); // REPLACE WITH PROPER LOGGING
      if (session != null) {
        session.disconnect();
      }
      if (channel != null) {
        channel.disconnect();
      }
    }
    return responseString;
  }

  private int fireAndRemember (PrintStream stream, ByteArrayOutputStream responseStream, String[] commands, int lowerBound, int upperBound
      , List<AclRoute> returnRoutes, AclExecution[] aclExecution, AclRouteExecution aclRouteExecution
      , String xxIpAddress, String yySubnetMask, String[] zzHost, AclResult[] aclResult, int waitTimeout, boolean countThem) throws InterruptedException {
	  log.info(String.format("fireAndRemember [%s]", xxIpAddress));
    int countCommands      = 0;
	String command         = "";
    String commandResponse = "";
    String[] answers       = null;
    AclRoute aclRoute      = null;
    for (int commandIndex=lowerBound; commandIndex<=upperBound; commandIndex++) {
      log.trace("xxIpAddress = " + xxIpAddress);
      log.trace("yySubnetMask = " + yySubnetMask);

      command = commands[commandIndex];
      command = command.replaceAll(CIDR_XXXXXXXX, xxIpAddress);
      command = command.replaceAll(CIDR_YYYYYYYY, yySubnetMask);
      command = command.replaceAll(CIDR_ZZZZZZZZ, zzHost[0]);
      aclExecution[0] = new AclExecution(command, SSH_NOT_EXEC);
      log.info("fireAndRemember() command = " + command);
      //responseStream.reset();
      stream.println(command);
      if (countThem) countCommands++;
      if (command.indexOf(ACL_PERMIT)>0 && command.indexOf(ACL_PERMIT)<5) continue; // Don't flush the stream here for permit/no permit commands
      stream.flush();
      commandResponse = waitForPrompt(responseStream, command, waitTimeout); 

      answers = commandResponse.split(NEWLINE); 
      for (String answer: answers) {
        if (answer.length()>1) log.info("fireAndRemember() answer = " + answer);
        try {
          aclRoute = Cidr.getRoute(answer);
          if (aclRoute.getSource_cidr() == null || aclRoute.getDestination_host() == null) {
        	  log.info("Bad route");
        	  continue;
          }
          returnRoutes.add(aclRoute);
          log.info("fireAndRemember: " + aclRoute.getSource_cidr() + ": " + aclRoute.getDestination_host());
        } catch (Exception e) {
          continue;
        }
      }

      aclExecution[0].setAnswer(commandResponse);
      aclRouteExecution = new AclRouteExecution(aclRoute, aclExecution[0]);
      aclResult[0].addRouteExecution(aclRouteExecution);
    }
    return countCommands;
  }


  private int execPostOrDeleteSshCommands(Conversation conversation, String aclId, AclPayload payload, String actionPrefix) 
		  throws SystemException {
    log.info("");   
    String commandPromptMaybe = conversation.getResponseStream().toString();
    log.info("execPostOrDeleteSshCommands("+(actionPrefix.length()==0?"POST":"DELETE") + ")" + " " + conversation);
    String lastCommandResult = "";
    String command01_conf_t = "conf t";
    String command02_ip_access_list_extended = String.format("ip access-list extended %s", aclId);
    String command03_permit_ip_host = String.format("%s permit ip %s %s host %s", actionPrefix, CIDR_XXXXXXXX, CIDR_YYYYYYYY, CIDR_ZZZZZZZZ);
    String command04_05_exit = "exit";
    //      String command06_sh_access_list = String.format("sh access-list %s | inc %s", aclId, CIDR_ZZZZZZZZ);
//    String command06_sh_access_list = String.format("sh access-list %s", aclId);

    String[] commands = new String[5];
    commands[0] = command01_conf_t;
    commands[1] = command02_ip_access_list_extended;
    commands[2] = command03_permit_ip_host;
    commands[3] = command04_05_exit;
    commands[4] = command04_05_exit;
//    commands[5] = command06_sh_access_list; 
    String commandResponse = null;

    Session session                        = conversation.getSession();
    ChannelShell channel                   = conversation.getChannel();
    ByteArrayOutputStream responseStream   = conversation.getResponseStream();
    responseStream.reset();
    PrintStream stream                     = conversation.getStream();
    AclResult[] aclResult                  = new AclResult[1];
    aclResult[0]                           = new AclResult();
    AclRoute aclRoute                      = null;
    List<AclRoute> returnRoutes            = null;
    AclExecution[] aclExecution            = new AclExecution[1]; 
    aclExecution[0]                        = new AclExecution(); 
    AclRouteExecution aclRouteExecution    = null;
    String xxIpAddress                     = ""; 
    String xxIpAddressWSubnetMask          = ""; 
    String yySubnetMask                    = ""; 
    String[] zzHost                        = new String[1]; 
    zzHost[0]                              = "";

    int commandIndex = 0;
    int countCommands = 0;
    try {
      fireAndRemember (stream, responseStream, commands, INDEX_ZERO, INDEX_ONE
          , returnRoutes, aclExecution, aclRouteExecution
          , xxIpAddress, yySubnetMask, zzHost, aclResult, TEN, false);

      for (AclRoute route: payload.getRoutes()) {          
        aclRoute = route;
        if (route.getSource_cidr()==null || route.getDestination_host()==null) 
        	continue;
        	//throw new InvalidInputException("Bad route specification: source_cidr=[" + route.getSource_cidr() + "]; destination_host=[" + route.getDestination_host() + "]");
        xxIpAddressWSubnetMask = route.getSource_cidr();
        yySubnetMask = Cidr.getInverseSubnetMask(xxIpAddressWSubnetMask);
        xxIpAddress = xxIpAddressWSubnetMask.substring(0, xxIpAddressWSubnetMask.indexOf('/'));
        if (!Cidr.isValidIpAddress(xxIpAddress, yySubnetMask)) {
        	throw new InvalidInputException(Cidr.MESSAGE_BAD_IP_ADDRESS + ": " + xxIpAddressWSubnetMask );
        }
        
        zzHost[0] = route.getDestination_host();

        countCommands += fireAndRemember (stream, responseStream, commands, INDEX_TWO, INDEX_TWO // One command against multiple routes
            , returnRoutes, aclExecution, aclRouteExecution
            , xxIpAddress, yySubnetMask, zzHost, aclResult, WAIT_TIMEOUT_PUT, true);
      }

      if (commands[INDEX_TWO].indexOf(ACL_PERMIT)<5) { // which it ALWAYS SHOULD HERE
          stream.flush();
          waitForPrompt(responseStream, commands[INDEX_TWO], WAIT_TIMEOUT_PUT); 
      }
      
      responseStream.reset(); 
      // Wait until the above commands have finished processing:
      for (int i=0; i<ONE_THOUSAND; i++) {
    	  Thread.sleep(ONE_THOUSAND);
    	  commandResponse = new String(responseStream.toByteArray());
    	  if (commandResponse.length() == 0) break;
    	  log.info("commandResponse = " + commandResponse);
    	  responseStream.reset();
      }
      
      fireAndRemember (stream, responseStream, commands, INDEX_THREE, INDEX_FOUR
          , returnRoutes, aclExecution, aclRouteExecution
          , xxIpAddress, yySubnetMask, zzHost, aclResult, WAIT_TIMEOUT_PUT, false);

    } catch (Exception x) {
    	throw new SystemException (SOMETHING_WRONG + x.getMessage());
    } 
    return countCommands; 
  }


  private List<AclRoute> execGetSshCommands(Conversation conversation, String aclId) {
    log.info("");    
    log.info(String.format("execGetSshCommands(%s, [%s])", aclId, conversation));
    List<AclRoute> returnRoutes = new ArrayList<>();

    String lastCommandResult = "";
    String command0_term_len_0 = "term len 0";
    String command01_sh_access_list = String.format("sh access-list %s", aclId);

    String[] commands = new String[2];
    commands[0] = command0_term_len_0;
    commands[1] = command01_sh_access_list;

    Session session                        = conversation.getSession();
    ChannelShell channel                   = conversation.getChannel();
    ByteArrayOutputStream responseStream   = conversation.getResponseStream();
    PrintStream stream                     = conversation.getStream();
    AclResult[] aclResult                  = new AclResult[1];
    aclResult[0]                           = new AclResult();
    AclRoute aclRoute                      = null;
    AclExecution[] aclExecution            = new AclExecution[1]; 
    aclExecution[0]                        = new AclExecution(); 
    AclRouteExecution aclRouteExecution    = null;
    String xxIpAddress                     = ""; 
    String yySubnetMask                    = ""; 
    String[] zzHost                        = new String[1]; 
    zzHost[0]                              = "";

    int commandIndex = 0;
    try {
      responseStream.reset();
      fireAndRemember (stream, responseStream, commands, INDEX_ZERO, INDEX_ONE
          , returnRoutes, aclExecution, aclRouteExecution
          , xxIpAddress, yySubnetMask, zzHost, aclResult, WAIT_TIMEOUT_GET, false);
    } catch (Exception x) {
      aclExecution[0].setAnswer(SSH_FAIL + "Exception: " + x.getMessage());
      //        aclRouteExecution = new AclRouteExecution(aclRoute, aclExecution[0]);
      //        aclResult[0].addRouteExecution(aclRouteExecution);
      //        routes.add(aclRoute);
    } 
    return returnRoutes; 
  }


  private List<AclRoute> execGetSshCommands(String username, String password, 
      String host, int port, String aclId, String[] returnMessage) {
    log.info("");
    log.info("CiscoCliAdaptorService.execGetSshCommands()");

    List<AclRoute> routes = new ArrayList<>();
    String command01_sh_access_list = String.format("sh access-list %s", aclId);

    Session session                     = null;
    ChannelExec  channel                = null;
    AclResult aclResult                 = new AclResult();
    AclRoute aclRoute                   = null;
    AclExecution aclExecution           = new AclExecution(); // To avoid NullPointerException if connection fails
    AclRouteExecution aclRouteExecution = null;
    String[] answers                    = null;
    List<String> answerIpAddresses      = new ArrayList<>();

    try {
      // Session
      session = new JSch().getSession(username, host, port);
      session.setPassword(password);
      session.setConfig(STRICT_HOSTKEY_CHECKING, STRICT_HOSTKEY_CHECKING_NO);
      session.connect(); // May throw up
      channel = (ChannelExec) session.openChannel(EXEC);
      String commandResponse = "";
      String command = "";

      // Commands
      command = command01_sh_access_list;
      aclExecution = new AclExecution(command, SSH_NOT_EXEC);
      channel.setCommand(command);
      returnMessage[0] = command + "\r\n";
      ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
      channel.setOutputStream(responseStream);
      channel.connect(); // ONLY ONCE
      while (channel.isConnected()) {
        Thread.sleep(100);
      }
      commandResponse = new String(responseStream.toByteArray());
      aclExecution.setAnswer(commandResponse);
      returnMessage[0] += "Answer: \r\n" + commandResponse;
      answers = commandResponse.split(NEWLINE); 
      for (String answer: answers) {
        if (answer.length()>0) log.info("answer = " + answer);
        try {
          aclRoute = Cidr.getRoute(answer);
        } catch (Exception e) {
          continue;
        }
        routes.add(aclRoute);
      }
      aclRouteExecution = new AclRouteExecution(aclRoute, aclExecution);
      aclResult.addRouteExecution(aclRouteExecution);

    } catch (Exception x) {
      aclExecution.setAnswer(SSH_FAIL + "Exception: " + x.getMessage());
      aclRouteExecution = new AclRouteExecution(aclRoute, aclExecution);
      aclResult.addRouteExecution(aclRouteExecution);
    } finally {
      if (session != null) {
        session.disconnect();
      }
      if (channel != null) {
        channel.disconnect();
      }
    }
    return routes;

  }


	private CodeStatusMessage processPutPostDeleteAcl(String aclId, AclPayload payload, String action)
			throws LoginException, InvalidInputException, SystemException, RecordNotFoundException {
		log.info(String.format("processPutPostDeleteAcl [%s] [%s]", aclId, action));
		if (!Cidr.isValidAclId(aclId))
			throw new InvalidInputException(Cidr.MESSAGE_BAD_ACL_ID);
		CodeStatusMessage returnObj = new CodeStatusMessage();
		Conversation conversation = Conversation.getNewConversation(user, pass, host, port);
		if (conversation == null)
			throw new LoginException(CONVERSATION_UNABLE);
		AclPayload theDeletePayload = new AclPayload();
		AclPayload thePostPayload = new AclPayload();
		List<AclRoute> theDeleteRoutes = new ArrayList<>();
		;
		List<AclRoute> thePostRoutes = new ArrayList<>();
		;
		AclPayload theGetPayload = getAclRoutes(aclId, conversation);
		Map<String, AclRoute> theGetRouteMap = theGetPayload.getRouteMap();
		switch (action) {
		case ACTION_PUT: {
			AclPayload thePutPayload = payload;
			Map<String, AclRoute> thePutRouteMap = thePutPayload.getRouteMap();

			log.info("theGetPayload counts " + theGetPayload.getRoutes().size());
			for (AclRoute route : theGetPayload.getRoutes()) {
				if (thePutRouteMap.get(route.getKey()) == null) { // If it exists, delete it
					theDeleteRoutes.add(route);
				}
			}
			log.info("Input Payload counts " + thePutPayload.getRoutes().size());
			for (AclRoute route : thePutPayload.getRoutes()) {
				if (theGetRouteMap.get(route.getKey()) == null) { // If it doesn't exist, add it
					thePostRoutes.add(route);
				}
			}
			theDeletePayload.setRoutes(theDeleteRoutes);
			thePostPayload.setRoutes(thePostRoutes);
			int commandsExecuted = 0;
			commandsExecuted = execPostOrDeleteSshCommands(conversation, aclId, theDeletePayload, ACTION_DELETE_PREFIX);
			String message = commandsExecuted + MESSAGE_NO_PERMIT + ACL_COMMANDS_EXECUTED + PHRASE_SEPARATOR;
			commandsExecuted = execPostOrDeleteSshCommands(conversation, aclId, thePostPayload, ACTION_POST_PREFIX);
			message += commandsExecuted + MESSAGE_PERMIT + ACL_COMMANDS_EXECUTED;
			message += PHRASE_SEPARATOR + (thePutRouteMap.size() - commandsExecuted) + SPACE + ENTRIES_ALREADY_EXIST;
			returnObj.setMessage(message);
			break;
		}
		case ACTION_POST: {
			thePostPayload = payload;
			for (AclRoute route : thePostPayload.getRoutes()) {
				if (theGetRouteMap.get(route.getKey()) == null) { // If it doesn't exist, add it
					thePostRoutes.add(route);
				}
			}
			int diff = thePostPayload.getRoutes().size() - thePostRoutes.size();
			thePostPayload.setRoutes(thePostRoutes);
			int commandsExecuted = 0;
			commandsExecuted = execPostOrDeleteSshCommands(conversation, aclId, thePostPayload, ACTION_POST_PREFIX);
			String message = commandsExecuted + MESSAGE_PERMIT + ACL_COMMANDS_EXECUTED;
			message += PHRASE_SEPARATOR + (diff) + SPACE + ENTRIES_ALREADY_EXIST;
			returnObj.setMessage(message);
			break;
		}
		case ACTION_DELETE: {
			theDeletePayload = payload;
			for (AclRoute route : theDeletePayload.getRoutes()) {
				if (theGetRouteMap.get(route.getKey()) != null) { // If it exists, delete it
					theDeleteRoutes.add(route);
				} else {
					throw new RecordNotFoundException(MESSAGE_RECORD_NOT_FOUND + route.getSource_cidr() + SPACE + HOST
							+ SPACE + route.getDestination_host());
				}
			}
			int commandsExecuted = 0;
			commandsExecuted = execPostOrDeleteSshCommands(conversation, aclId, theDeletePayload, ACTION_DELETE_PREFIX);
			String message = commandsExecuted + MESSAGE_NO_PERMIT + ACL_COMMANDS_EXECUTED + PHRASE_SEPARATOR;
			returnObj.setMessage(message);
			break;
		}
		}
		conversation.hangUp();
		return returnObj;
	}

  @Override
  public CodeStatusMessage postAcl (String aclId, AclPayload payload) throws LoginException, InvalidInputException, SystemException, RecordNotFoundException {
	  log.info(String.format("postAcl [%s]", aclId));
	  return processPutPostDeleteAcl (aclId, payload, ACTION_POST);
  }

  @Override
  public CodeStatusMessage deleteAcl (String aclId, AclPayload payload) throws LoginException, InvalidInputException, SystemException, RecordNotFoundException {
	  log.info(String.format("deleteAcl [%s]", aclId));
	  return processPutPostDeleteAcl (aclId, payload, ACTION_DELETE);
  }


  @Override
  public CodeStatusMessage putAcl (String aclId, AclPayload payload) throws LoginException, InvalidInputException, SystemException, RecordNotFoundException { 
	  log.info(String.format("putAcl [%s]", aclId));
	  return processPutPostDeleteAcl (aclId, payload, ACTION_PUT);
  }


  @Override
  public AclPayload getAclRoutes (String aclId, Conversation conversation) throws LoginException, InvalidInputException {
	  log.info(String.format("getAclRoutes [%s]", aclId));
	if (!Cidr.isValidAclId(aclId)) throw new InvalidInputException(Cidr.MESSAGE_BAD_ACL_ID);
	boolean sharedConversation = (conversation!=null);
	if  (!sharedConversation) conversation = Conversation.getNewConversation(user, pass, host, port);
    List<AclRoute> routes = new ArrayList<>();
    if (conversation != null) {
      //      private String execGetSshCommands(Conversation conversation, String aclId) {

      int retries = 10; // THIS IS A KLUDGE
      for (int i=0; i<retries; i++) {
        routes = execGetSshCommands(conversation, aclId);
        if (routes.size()>0) break;
      }
    } else {
      throw new LoginException (CONVERSATION_UNABLE);
    }
    if  (!sharedConversation) conversation.hangUp();
    AclPayload payload = new AclPayload();
    payload.setRoutes(routes);
    return payload;
  }


}


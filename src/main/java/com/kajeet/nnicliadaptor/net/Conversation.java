package com.kajeet.nnicliadaptor.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Optional;

import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class Conversation {
	
	public static final String STRICT_HOSTKEY_CHECKING    = "StrictHostKeyChecking"; // CONSIDER REMOVING TO INDEPENDENT UTIL/CONSTANTS CLASS
	public static final String STRICT_HOSTKEY_CHECKING_NO = "no";
	public static final String SHELL                      = "shell";

    Session session = null;
    ChannelShell channel = null;
    ByteArrayOutputStream responseStream   = new ByteArrayOutputStream();
    PrintStream stream;
    
	public Session getSession() {
		return session;
	}
	public void setSession(Session session) {
		this.session = session;
	}
	public ChannelShell getChannel() {
		return channel;
	}
	public void setChannel(ChannelShell channel) {
		this.channel = channel;
	}
	public ByteArrayOutputStream getResponseStream() {
		return responseStream;
	}
	public void setResponseStream(ByteArrayOutputStream responseStream) {
		this.responseStream = responseStream;
	}
	
	public PrintStream getStream() {
		return stream;
	}
	public void setStream(PrintStream stream) {
		this.stream = stream;
	}
	
	public static Conversation getNewConversation (String username, String password, String host, int port) {
		Conversation conversation = new Conversation();
		try {
			conversation.session = new JSch().getSession(username, host, port);
			conversation.session.setPassword(password);
			conversation.session.setConfig(STRICT_HOSTKEY_CHECKING, STRICT_HOSTKEY_CHECKING_NO);
			conversation.session.connect(); // May throw up
			conversation.channel = (ChannelShell) conversation.session.openChannel(SHELL);
			conversation.stream = new PrintStream(conversation.channel.getOutputStream());
			conversation.channel.setOutputStream(conversation.responseStream);
			conversation.channel.connect();
		} catch (JSchException x) {
			System.out.println("JSchException in net.Conversation.getNewConversation(): " + x.getMessage());
			conversation = null;
		} catch (IOException x) {
			conversation = null;
			System.out.println("IOException in net.Conversation.getNewConversation(): " + x.getMessage());
		}
		return conversation;
	}
	
	
	public void hangUp () {
        if (session != null) {
            session.disconnect();
        }
        if (channel != null) {
            channel.disconnect();
        }
    }


    
}
